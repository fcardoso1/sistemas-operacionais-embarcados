#include <unistd.h> /* For write() and STDOUT_FILENO */
#include <stdlib.h> /* For EXIT_SUCCESS and EXIT_FAILURE */

int main(void) {
        char hello[] = "Hello, World\n";
        
        /* Attempt to write `hello` to standard output file */
        write(STDOUT_FILENO, hello, sizeof(hello) - 1);

        return EXIT_SUCCESS; 
}
